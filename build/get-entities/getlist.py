#!/usr/bin/python
# This helper script takes the entities from build/get-entities/entities.inc,
# parses it and creates build/Entities. Make sure entities.inc 
# is updated to the latest values before running this script.
# Copyright 2014 Rupinder Singh Khokhar <rsk1coder99@gmail.com>

import sys
try:
	sys.stdout = open('../Entities', 'w')
	sys.stdin = open('entities.inc', 'r')
except:
	print "Please run the get-entities script befor running this one\n";
	sys.exit(1);
print "# Entities for HTML5\n# Note, some entities are allowed to omit their trailing semicolon, which is\n# why most entities here have semicolons after them.\n\n# Entity Code"
while 1:
	try:
		x = raw_input()
	except:
		break
	x = x.split(",");
	x[1] = x[1].split(" ")
	x[0] = x[0].split("\"")
	print x[0][1],
	for i in x[1]:
		try:
			if i[0] == '}':
				continue
			if i[0] == 'U' and i[1] == '+':
				print "0x"+i[2:],
			else:
				print i,
		except:
			continue
	print
